## 简介
借助于阿里云语音服务，实现的自动将语音转化为文本


## 使用

#### 1. 阿里云开通智能语音交互
智能语音交互网址： https://nls-portal.console.aliyun.com
添加一个项目，拿到这个刚添加项目的 Appkey（注意，采样率是16K）
![](//cdn.weiunity.com/site/341/news/2025458c20734b85b58d857bb563d29f.png)


#### 2. pom.xml 的 dependencies 中加入
````
<!-- 管雷鸣的工具包 -->
<dependency>
 	<groupId>com.xnx3.util</groupId>
	<artifactId>xnx3-util</artifactId>
	<version>1.0.0</version>
</dependency>

<!-- 阿里云智能语音 -->
<dependency>
    <groupId>com.alibaba.nls</groupId>
    <artifactId>nls-sdk-long-asr</artifactId>
    <version>2.0.3</version>
</dependency>
<dependency>
    <groupId>com.alibaba.nls</groupId>
    <artifactId>nls-sdk-short-asr</artifactId>
    <version>2.0.3</version>
</dependency>
      <dependency>
    <groupId>com.alibaba.nls</groupId>
    <artifactId>nls-sdk-tts</artifactId>
    <version>2.0.3</version>
</dependency>
<dependency>
    <groupId>com.aliyun</groupId>
    <artifactId>aliyun-java-sdk-core</artifactId>
    <version>3.7.1</version>
</dependency>
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>fastjson</artifactId>
    <version>1.2.49</version>
</dependency>
````

#### 3. 代码使用示例
````
package com.xnx3.nls;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import com.xnx3.BaseVO;
import com.xnx3.nls.aliyun.NlsUtil;

/**
 * 智能对话
 * @author 管雷鸣
 */
@Controller
@RequestMapping("/chat")
public class TestController{
	static{
		/* 
    	 * 先设置初始化的参数。这个在程序中只需设置一次即可。
    	 * accessKeyId 阿里云 AccessKeyID
		 * accessKeySecret 阿里云AccessKeySecret
		 * appKey 语音识别 appkey
    	 */
		NlsUtil.init("LTA.............", "dTuDCHVnZF....................", "JbhVJ2..........");
	}
	
	/**
	 * 语音问答 
	 * @param voice 用户说出的话，语音
	 * @return baseVO.info 回复的内容
	 * @throws IOException 
	 */
	@RequestMapping(value="/voiceTell.do", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO voiceTell(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("voice") MultipartFile voice) throws IOException{
		//允许跨域请求
		response.addHeader("Access-Control-Allow-Origin", "*");	
		//说出的声音转换为的文字
		String voiceText = NlsUtil.voiceToString(voice.getInputStream());
		
		return BaseVO.success(voiceText);
	}
	
}

````

#### 4. 前端html界面
参见 https://gitee.com/leimingyun/aichat