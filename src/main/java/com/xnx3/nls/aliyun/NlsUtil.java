package com.xnx3.nls.aliyun;

import com.alibaba.nls.client.AccessToken;
import com.alibaba.nls.client.protocol.InputFormatEnum;
import com.alibaba.nls.client.protocol.NlsClient;
import com.alibaba.nls.client.protocol.SampleRateEnum;
import com.alibaba.nls.client.protocol.asr.SpeechRecognizer;
import com.alibaba.nls.client.protocol.asr.SpeechRecognizerResponse;
import com.xnx3.DateUtil;
import com.alibaba.nls.client.protocol.asr.SpeechRecognizerListener;
import java.io.InputStream;

/**
 * 语音、文字之间的转换<br/>
 * 先调用 com.xnx3.nls.aliyun.NlsUtil.init(......) 设置初始化参数。这个在程序中只需设置一次即可。
 * @author 管雷鸣
 * @date 2019/7/30
 */
public class NlsUtil {
    private static String appKey;		//语音识别 appkey
    private static String accessKeyId;	//阿里云 AccessKeyID
    private static String accessKeySecret;	//阿里云AccessKeySecret
    
    private static String accessToken;
    private static long accessTokenExpireTime = 0;	//accesstoken 过期时间
    private NlsClient client;	//获取统一通过 getNlsClient()
    private String resultText;	//识别的结果。如果为null，则等待，直到值才返回
    
    /**
     * 初始化参数设置。只需设置一次即可
     * @param accessKeyId 阿里云 AccessKeyID
     * @param accessKeySecret 阿里云AccessKeySecret
     * @param appKey 语音识别 appkey
     */
    public static void init(String accessKeyId, String accessKeySecret, String appKey) {
    	NlsUtil.accessKeyId = accessKeyId;
    	NlsUtil.accessKeySecret = accessKeySecret;
    	NlsUtil.appKey = appKey;
	}

    /**
     * 将一句话语音转化为文字
     * @param ins springMVC接收的 @RequestParam("voice") MultipartFile voice ,可以直接使用 voice.getInputStream() 获取到
     * @param unknowText 未识别返回的字符串。如果阿里语音服务识别不出是什么，那么此方法会默认返回这里定义的字符串。比如这里可以填写：未能识别您说的是什么，请重新说一遍
     * @return 识别的结果，字符串。如果未识别，则返回空字符串 ""
     */
    public static String voiceToString(InputStream ins){
    	if(NlsUtil.accessKeyId == null){
    		System.out.println("Please first com.xnx3.nls.aliyun.NlsUtil.init(......) ");
    	}
    	if(ins == null){
    		return "";
    	}
    	NlsUtil nls = new NlsUtil();
    	nls.voiceToTextProcess(ins);
    	if(nls.resultText == null || nls.resultText.length() == 0){
        	return "";
        }
    	return nls.resultText;
    }
    
    public NlsClient getNlsClient(){
    	if(this.client == null){
    		// Step0 创建NlsClient实例,应用全局创建一个即可,默认服务地址为阿里云线上服务地址
    		this.client = new NlsClient(getAccessToken());
    	}
    	if(DateUtil.timeForUnix10() + 100L > accessTokenExpireTime){
    		//过期了，重新获取token，并创建client
    		this.client.shutdown();
    		this.client = null;
    		this.client = new NlsClient(getAccessToken());
    	}
    	return this.client;
    }
    
    /**
     * 获取 AccessToken
     */
    public static String getAccessToken(){
    	if(DateUtil.timeForUnix10() + 100L > accessTokenExpireTime){
    		//过期了，重新获取token

            try {
                AccessToken at = AccessToken.apply(NlsUtil.accessKeyId, NlsUtil.accessKeySecret);
                accessToken = at.getToken();
                accessTokenExpireTime = at.getExpireTime();
            } catch (Exception e) {
                e.printStackTrace();
            }
    	}
    	
    	return accessToken;
    }
    
    
    private SpeechRecognizerListener getRecognizerListener() {
        SpeechRecognizerListener listener = new SpeechRecognizerListener() {
            // 识别出中间结果.服务端识别出一个字或词时会返回此消息.仅当setEnableIntermediateResult(true)时,才会有此类消息返回
            @Override
            public void onRecognitionResultChanged(SpeechRecognizerResponse response) {
//                // 事件名称 RecognitionResultChanged
//                System.out.println("识别出中间结果  name: " + response.getName() +
//                        // 状态码 20000000 表示识别成功
//                        ", status: " + response.getStatus() +
//                        // 一句话识别的中间结果
//                        ", result: " + response.getRecognizedText());
            }

            // 识别完毕
            @Override
            public void onRecognitionCompleted(SpeechRecognizerResponse response) {
                resultText = response.getRecognizedText();
            }
        };
        return listener;
    }

    /**
     * 将一句话语音转化为文字
     * @param ins springMVC接收的 @RequestParam("voice") MultipartFile voice ,可以直接使用 voice.getInputStream() 获取到
     */
    public void voiceToTextProcess(InputStream ins) {
        SpeechRecognizer recognizer = null;
        try {
            // Step1 创建实例,建立连接
            recognizer = new SpeechRecognizer(client, getRecognizerListener());
            recognizer.setAppKey(appKey);
            // 设置音频编码格式
            recognizer.setFormat(InputFormatEnum.PCM);
            // 设置音频采样率
            recognizer.setSampleRate(SampleRateEnum.SAMPLE_RATE_16K);
            // 设置是否返回中间识别结果
            recognizer.setEnableIntermediateResult(true);

            // Step2 此方法将以上参数设置序列化为json发送给服务端,并等待服务端确认
            recognizer.start();
            // Step3 语音数据来自声音文件用此方法,控制发送速率;若语音来自实时录音,不需控制发送速率直接调用 recognizer.sent(ins)即可
            recognizer.send(ins, 6400, 200);
            // Step4 通知服务端语音数据发送完毕,等待服务端处理完成
            recognizer.stop();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // Step5 关闭连接
            if (null != recognizer) {
                recognizer.close();
            }
        }
    }
    
    public void shutdown() {
    	getNlsClient().shutdown();
    }


    
    public static void main(String[] args) {
    	/* 
    	 * 先设置初始化的参数。这个在程序中只需设置一次即可。
    	 * accessKeyId 阿里云 AccessKeyID
	     * accessKeySecret 阿里云AccessKeySecret
	     * appKey 语音识别 appkey
    	 */
    	NlsUtil.init("LTA.............", "dTuDCHVnZF....................", "JbhVJ2..........");
    	System.out.println(NlsUtil.voiceToString(null));
    }

}
